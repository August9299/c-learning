﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace HelloWorldWcfServiceLibrary
{
    public class HelloWorldService : IHelloWorldService
    {
        private static Image СreateImage(string text)
        {
            Image img = new Bitmap(1, 1);
            Graphics drawing = Graphics.FromImage(img);

            //measure the string to see how big the image needs to be
            
            Font font = new Font(FontFamily.GenericSansSerif,
            25.0F, FontStyle.Bold);
            SizeF textSize = drawing.MeasureString(text, font);

            //free up the dummy image and old graphics object
            img.Dispose();
            drawing.Dispose();

            //create a new image of the right size
            img = new Bitmap((int)textSize.Width, (int)textSize.Height);
            drawing = Graphics.FromImage(img);
            drawing.Clear(Color.Black);
            Brush textBrush = new SolidBrush(Color.Crimson);
            drawing.DrawString(text, font, textBrush, 0, 0);
            drawing.Save();

            textBrush.Dispose();
            drawing.Dispose();

            return img;
        }

        public Stream GetHelloWorldStream(string clientName)
        {
            try
            {
                var stream = new MemoryStream();
                Image image = СreateImage($"Hello World from\nclient '{clientName}'!");
                image.Save(stream, ImageFormat.Gif);               
                stream.Position = 0;
                return stream;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return null;
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
