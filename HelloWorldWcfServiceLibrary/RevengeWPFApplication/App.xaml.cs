﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace RevengeWPFApplication
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            // Process unhandled exception

            // Prevent default unhandled exception processing
            e.Handled = true;
        }



        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            DispatcherUnhandledException += ((sender, args) =>
            {
                // TODO: Remove bydlo-code
                var f = File.Open("unhandled-exceptions.log", FileMode.Create, FileAccess.Write);
                string message = $"----\nsender: {sender}\nargs: {args.Exception}\n";
                var sw = new StreamWriter(f);
                sw.Write(message);
                f.Flush(true);
                sw.Close();
                f.Close();
                sw.Dispose();
                f.Dispose();
            });
        }
    }
}
