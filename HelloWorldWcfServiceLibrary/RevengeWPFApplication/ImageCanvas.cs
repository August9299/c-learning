﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace RevengeWPFApplication
{
    class ImageCanvas : Canvas
    {
        public Logger Logger { get; set; }
        public bool IsDownloading { get; private set; }
        private BitmapImage _image;


        protected override void OnRender(DrawingContext dc)
        {
            if (IsDownloading)
            {
                dc.DrawText(new FormattedText("Loading...", CultureInfo.CurrentCulture, FlowDirection.LeftToRight,
                        new Typeface("Arial"), 20, new SolidColorBrush(Color.FromRgb(0, 0, 0))), new Point(10, 50));
            }     
            if (!IsDownloading && _image != null)
            {                
                dc.DrawImage(_image, new Rect(0, 0, _image.PixelWidth, _image.PixelHeight));                
            }
        }

        public void ShowLoadingMessage()
        {
            IsDownloading = true;
            InvalidateVisual();
        }

        public void LoadImage(Stream stream)
        {
            if (!stream.CanRead)
            {
                MessageBox.Show("Не получается прочитать stream");
                return;
            }

            var byteStream = new MemoryStream();
            stream.CopyTo(byteStream);
            Logger.Log($"{byteStream.Length} скопировано");
            byteStream.Position = 0;

            //byte[] buffer = new byte[1024*1024];//stream.Length];
            //int totalLength = stream.Read(buffer, 0, buffer.Length);
            //MessageBox.Show($"Считано {totalLength} байт ({totalLength/1024} Кб) [{stream.CanRead}]");
            //var byteStream = new MemoryStream(buffer,0,totalLength);
            //stream.CopyTo(byteStream);

            _image = new BitmapImage();         
            _image.BeginInit();
            _image.CacheOption = BitmapCacheOption.OnLoad;
            _image.StreamSource = byteStream;
            _image.EndInit();

            byteStream.Close();
            stream.Close();

            IsDownloading = false;
            InvalidateVisual();
        }
    }
}
