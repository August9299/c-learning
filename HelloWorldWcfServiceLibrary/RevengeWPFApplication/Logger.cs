﻿using System;
using System.IO;

namespace RevengeWPFApplication
{
    public class Logger : IDisposable
    {
        private FileStream _file;
        private StreamWriter _writer;

        public Logger(string filename)
        {
            _file = File.Create(filename);
            _writer = new StreamWriter(_file);
        }

        public void Log(string message)
        {
            _writer.Write(message + "\n");
        }

        public void Dispose()
        {
            if (_writer != null)
            {
                _writer.Dispose();
            }
            if (_file != null)
            {
                _file.Close();
                _file.Dispose();
            }
        }

        public void Log(bool value)
        {
            Log(value? "true" : "false");
        }
    }
}