﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using RevengeWPFApplication.HWServiceReference;

namespace RevengeWPFApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {        
        private Logger _logger = new Logger("revenge-app.log");
        public MainWindow()
        {            
            InitializeComponent();
            canvasI1.Logger = _logger;
            Closed += (sender, args) => _logger.Dispose();            
        }        
  

        private void button_Click(object sender, RoutedEventArgs e)
        {            
            DoDownload();
        }

        private async void DoDownload()
        {
            button.IsEnabled = false;
            canvasI1.ShowLoadingMessage();

            await Dispatcher.InvokeAsync(() =>
            {
                try
                {
                    var client = new HelloWorldServiceClient();
                    Stream s = client.GetHelloWorldStream("Revenge");
                    canvasI1.LoadImage(s);                    
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.ToString());
                }
            });            
            button.IsEnabled = true;
        }
    }
}
