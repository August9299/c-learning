﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using HelloWorldWcfServiceLibrary;

namespace ServiceRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var host = new ServiceHost(typeof(HelloWorldService)))
            {
//                TcpTransportBindingElement transport = new TcpTransportBindingElement();
//                transport.TransferMode = TransferMode.Streamed;
//                BinaryMessageEncodingBindingElement encoder = new BinaryMessageEncodingBindingElement();
//                CustomBinding binding = new CustomBinding(encoder, transport);
                

                host.Open();
                Console.WriteLine("Service started. Press Enter to halt");
                Console.ReadLine();
                host.Close();
            }
        }
    }
}
