﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace JSONSerializer
{
    public enum JsonSerializerSettings
    {
        SerialiseAllObjects,
        SerializeOnlyObjectsWithSerialiseAttribute
    }

    public class JsonSerializer
    {        
        private readonly JsonSerializerSettings _settings;

        public JsonSerializer(JsonSerializerSettings settings = JsonSerializerSettings.SerialiseAllObjects)
        {            
            _settings = settings;
        }

        public void Serialize(Stream stream, object obj)
        {            
            using (StreamWriter streamWriter = new StreamWriter(stream))
            {
                SerializeObject(obj, streamWriter);
            }            
        }

        private void SerializeObject(object obj, StreamWriter streamWriter)
        {
            Type objectType = obj.GetType();
            if (ObjectIsAllowedToSerialize(objectType))
            {
                var jsonWriter = new JsonWriter(streamWriter);
                jsonWriter.WriteObjectStart();
                                
                foreach (FieldInfo fieldInfo in GetAllFields(objectType))
                {
                    if (!FieldHasNonSerializedAttribute(fieldInfo))
                    {                        
                        if (fieldInfo.GetValue(obj) == null)
                        {
                            jsonWriter.WriteNull(fieldInfo.Name);
                        }
                        else if (fieldInfo.FieldType == typeof (string))
                        {                            
                            jsonWriter.WriteString(fieldInfo.Name, (string)fieldInfo.GetValue(obj));
                        }
                        else if (fieldInfo.FieldType.IsArray)
                        {
                            SerializeArray(fieldInfo.Name, (Array)fieldInfo.GetValue(obj), jsonWriter, streamWriter);
                        }
                        else if (fieldInfo.FieldType.IsClass)
                        {
                            jsonWriter.WriteKey(fieldInfo.Name);
                            SerializeObject(fieldInfo.GetValue(obj), streamWriter);
                        }                        
                        else
                        {
                            jsonWriter.WriteKeyValue(fieldInfo.Name, fieldInfo.GetValue(obj));
                        }
                    }
                }
                jsonWriter.WriteObjectEnd();
            }
        }

        private void SerializeArray(string name, Array array, JsonWriter jsonWriter, StreamWriter streamWriter)
        {
            jsonWriter.WriteArrayStart(name);   
            
            JsonWriter arrayJsonWriter = new JsonWriter(streamWriter);         

            for (int i = 0; i < array.Length; i++)
            {
                var value = array.GetValue(i);
                if (value == null)
                {
                    arrayJsonWriter.WriteNull();
                }
                else if (value.GetType().IsClass)
                {                    
                    if (value is string)
                    {
                        arrayJsonWriter.WriteString((string)value);
                    }
                    else
                    {
                        arrayJsonWriter.CheckForDelimeter();
                        SerializeObject(value, streamWriter);
                    }

                }
                else
                {
                    arrayJsonWriter.WriteValue(value);
                }
            }
                                   
            jsonWriter.WriteArrayEnd();
        }


        private IEnumerable<FieldInfo> GetAllFields(Type type)
        {
            return 
                type.GetFields(BindingFlags.Public | BindingFlags.Instance).Concat(
                type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance)
                );
        }


        private bool ObjectIsAllowedToSerialize(Type objectType)
        {
            bool serializableKey = (objectType.Attributes & TypeAttributes.Serializable) == TypeAttributes.Serializable;
            if (this._settings == JsonSerializerSettings.SerialiseAllObjects ||
                (this._settings == JsonSerializerSettings.SerializeOnlyObjectsWithSerialiseAttribute && serializableKey))
            {
                return true;
            }
            return false;
        }
        
        

        private static bool FieldHasNonSerializedAttribute(FieldInfo info)
        {                        
            return (info.Attributes & FieldAttributes.NotSerialized) == FieldAttributes.NotSerialized;
        }
    }
}