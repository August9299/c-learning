﻿using System;
using System.IO;

namespace JSONSerializer
{
    internal class JsonWriter
    {
        private const char ObjectStartSymbol = '{';
        private const char ObjectEndSymbol = '}';
        private const char ArrayStartSymbol = '[';
        private const char ArrayEndSymbol = ']';
        private const char DelimiterSymbol = ',';
        private const string NumberDecimalSeparator = ".";
        private const string NullSequence = "null";

        private bool _firstElement = true;
        private readonly StreamWriter _streamWriter;

        public JsonWriter(StreamWriter streamWriter)
        {
            _streamWriter = streamWriter;

            
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = NumberDecimalSeparator;
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            // TODO: return NumberDecimalSeparator to comma when JSONserialization ends
        }

        internal void WriteKeyValue(string key, object value)
        {
            CheckForDelimeter();            
            _streamWriter.Write($"\"{key}\":{value}");            
        }
        

        internal void WriteString(string key, string value)
        {
            CheckForDelimeter();            
            _streamWriter.Write($"\"{key}\":\"{value}\"");
        }

        internal void WriteString(string value)
        {
            CheckForDelimeter();
            _streamWriter.Write($"\"{value}\"");
        }

        public void CheckForDelimeter()
        {
            if (!_firstElement)
            {
                _streamWriter.Write(DelimiterSymbol);
            }
            else
            {
                _firstElement = false;
            }
        }

        internal void WriteObjectEnd()
        {
            _streamWriter.Write(ObjectEndSymbol);
        }

        internal void WriteObjectStart()
        {
            _streamWriter.Write(ObjectStartSymbol);
        }
        

        public void WriteArrayStart(string key)
        {
            CheckForDelimeter();
            _streamWriter.Write($"\"{key}\":{ArrayStartSymbol}");
        }

        public void WriteArrayEnd()
        {
            _streamWriter.Write(ArrayEndSymbol);
        }

        public void WriteNull(string key = null)
        {
            CheckForDelimeter();
            if (key != null)
            {
                _streamWriter.Write($"\"{key}\":");
            }
            _streamWriter.Write(NullSequence);
        }
        

        public void WriteKey(string key)
        {
            CheckForDelimeter();
            _streamWriter.Write($"\"{key}\":");
            
        }       

        internal void WriteTest()
        {
            _streamWriter.Write("@");
        }

        public void WriteValue(object value)
        {
            CheckForDelimeter();
            _streamWriter.Write(value);
        }
    }
}