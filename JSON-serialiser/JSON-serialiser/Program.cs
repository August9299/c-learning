﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSONSerializer
{
    class Program
    {
        public static void Main(string[] args)
        {                        
            var fileStream = new FileStream("object.json", FileMode.Create);
            using (fileStream)
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(fileStream, new TestObject());                
            }                       
        }
    }
}
