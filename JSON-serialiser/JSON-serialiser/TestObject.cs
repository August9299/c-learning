﻿using System;

namespace JSONSerializer
{
    // A test object that needs to be serialized.    
    public class TestObject
    {

        public int Member1 = 11;
        private string _member2 = "hello :p";
        public string Member3 = "Hello!";
        public double Member4 = Math.PI;
        public int[] ints = {10, 20, 30, 44};
        public double[] doubles = { 10.1, 20.1, 30.1, 44.1 };

        [NonSerialized()] public string Member5 = "ignore it";
        public string MemberNull = null;      
        
        InnerClass _innerClass = new InnerClass();
        private InnerClass[] _innerClasses = new InnerClass[3];

        public TestObject()
        {
            _innerClasses[0] = new InnerClass();
            _innerClasses[1] = null;
            _innerClasses[2] = new InnerClass();
        }

        private int endMember = 666;
    }


    public class InnerClass
    {
        public string name = "InnerClass";
        public string[] strings = {"s1", "s2", "s3", "s4", "s5", "s6"};
    }
}