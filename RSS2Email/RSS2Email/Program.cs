﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace RSS2Email
{
    class Program
    {
        private const string ExampleRss = "https://twitrss.me/twitter_user_to_rss/?user=Vladimi64650052";
        private const string ExampleEmail = "acc.wda@gmail.com";
        private const string DefaultLocalStorage = "RSS.tmp";

        static void Main(string[] args)
        {           
            string source = args.Length > 0 ? args[0] : ExampleRss;
            string email = args.Length > 1 ? args[1] : ExampleEmail;
            string storage = args.Length > 2 ? args[2] : DefaultLocalStorage;

            using (RssContainer container = new RssContainer(storage))
            {
                container.LoadNewRss(source);                
                var rssToEmail = container.GetDifferenceWithLocal();

                if (!rssToEmail.Empty)
                {
                    Console.WriteLine("Есть обновления в RSS, отсылаем");
                    EmailUtyls.SendEmail(email, rssToEmail);
                    container.SaveNewRss();
                    Console.WriteLine("Rss сохранён в файл");
                }
                else
                {
                    Console.WriteLine("Ничего нового");
                }
            }
        }
    }    
}
