﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;

namespace RSS2Email
{
    public class RssContainer : IDisposable
    {
        private RssList _newRss, _oldRss;
        private FileStream _localStorageFileStream;

        public RssContainer(string localStoragePath)
        {
            _localStorageFileStream = File.Open(localStoragePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            LoadOldRss();
        }


        public int LoadNewRss(string source)
        {            
            string xml = new WebClient().DownloadString(source);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            int count = 0;
            _newRss = new RssList();
            foreach (XmlNode xmlItem in doc.GetElementsByTagName("item"))
            {
                count++;
                RssItem item = RssItem.Load(xmlItem);
                _newRss.Items.Add(item);
            }
            return count;
        }

        
        private void LoadOldRss()
        {
            BinaryFormatter bf = new BinaryFormatter();
            try
            {
                _oldRss = (RssList) bf.Deserialize(_localStorageFileStream);
            }
            catch (SerializationException)
            {                
                CleanFile();
                _oldRss = new RssList();
            }
        }

        public RssList GetDifferenceWithLocal()
        {
            return new RssList {Items = _newRss.Items.Except(_oldRss.Items).ToList()};            
        }

        public void SaveNewRss()
        {
            CleanFile();
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(_localStorageFileStream, _newRss);
        }

        private void CleanFile()
        {
            _localStorageFileStream.SetLength(0);
            _localStorageFileStream.Flush();
        }

        public void Dispose()
        {
            _localStorageFileStream.Dispose();
        }
    }    
}