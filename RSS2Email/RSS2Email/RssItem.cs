﻿using System;
using System.Xml;

namespace RSS2Email
{    
    [Serializable]
    public struct RssItem
    {
        private static readonly string TitleNodeName = "title";
        private static readonly string DescriptionNodeName = "description";
        private static readonly string AuthorNodeName = "dc:creator";
        private static readonly string DateNodeName = "pubDate";
        private static readonly string GuidNodeName = "guid";
        private static readonly string LinkNodeName = "link";
        private static string NotSetMessage =  "(not set)";

        public string Title { get; set; }
        public string Author { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string Guid { get; set; }
        public string Link { get; set; }


        public static RssItem Load(XmlNode xmlNode)
        {
            var xml = xmlNode.OuterXml;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            return new RssItem
            {
                Title = findValueInXmlDocument(doc, TitleNodeName),
                Description = findValueInXmlDocument(doc, DescriptionNodeName),
                Author = findValueInXmlDocument(doc, AuthorNodeName),
                Date = findValueInXmlDocument(doc, DateNodeName),
                Guid = findValueInXmlDocument(doc, GuidNodeName),
                Link = findValueInXmlDocument(doc, LinkNodeName)
            };
        }

        private static string findValueInXmlDocument(XmlDocument doc, string nodeName)
        {
            var elementsByTagName = doc.GetElementsByTagName(nodeName);
            if (elementsByTagName.Count > 0)
                return elementsByTagName[0].InnerText;
            return NotSetMessage;
        }

        public override string ToString()
        {
            return $"Title: {Title}\n" +
                   $"Author: {Author}\n" +
                   $"Publication date: {Date}\n" +
                   $"Link: {Link}\n" +
                   $"Description: {Description}";
        }
        
    }
}