﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RSS2Email
{
    [Serializable]
    public class RssList
    {
        public List<RssItem> Items = new List<RssItem>();
        public bool Empty => Items.Count == 0;

        // TODO: Add update date

        public override string ToString()
        {
            string result = string.Empty;
            int count = 1;
            return 
                Items.Aggregate(result, (current, rssItem) => current + $"========== {count++} ==========\n{rssItem}\n");
        }
    }
}