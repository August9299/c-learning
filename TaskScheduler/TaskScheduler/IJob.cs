﻿namespace TaskScheduler
{
    public interface IJob
    {
        void Execute(object parameters);
    }
}