﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TaskScheduler
{
    class Program
    {
        static void Main(string[] args)
        {
            TaskScheduler scheduler = new TaskScheduler();

            scheduler.SchedulePeriodicJob(new TestJob(), new TimeSpan(0, 0, 0, 2), "bla-bla");
            
            TaskScheduler.Task task = scheduler.ScheduleDelayedJob(new HardJob(), new TimeSpan(0, 0, 0, 1));
            
            scheduler.ScheduleAction(() => Console.WriteLine("!"), new TimeSpan(0,0,0,10), true);

            task.Cancel();

            Console.ReadLine();
        }
    }
}
