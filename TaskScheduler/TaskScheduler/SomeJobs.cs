﻿using System;
using System.Threading;

namespace TaskScheduler
{
    public class TestJob : IJob
    {
        public void Execute(object parameters)
        {
            Console.WriteLine($"{nameof(TestJob)} executed! Message: {parameters}");
        }
    }

    public class HardJob : IJob
    {
        public void Execute(object parameters)
        {
            Console.WriteLine("Hard job started");
            Thread.Sleep(5000);
            Console.WriteLine("Hard job ended");
        }
    }
}