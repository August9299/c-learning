﻿using System;
using System.Collections;
using System.Timers;

namespace TaskScheduler
{
    public class TaskScheduler
    {
        /* Class - wrapper over the System.Timers.Timer 
         * (Maybe trash class)
         */
        public class Task
        {
            private readonly Timer _timer;
            public bool Repeatable { get; set; }
            public bool Actual => _timer.Enabled;

            public Task(Action action, double interval, bool repeatable)
            {
                _timer = new Timer(interval);
                Repeatable = repeatable;
                _timer.Elapsed += (sender, args) =>
                {
                    if (!Repeatable)
                    {
                        _timer.Stop();
                    }
                    action();
                };
            }

            public void Cancel() => _timer.Enabled = false;
            public void Launch() => _timer.Enabled = true;
            public double Interval
            {
                get { return _timer.Interval; }
                set { _timer.Interval = value; }
            }
        }

        // TODO: Add the ability to store a list of running tasks
        //private List<Task> _tasks = new ArrayList();

        public Task ScheduleDelayedJob(IJob job, TimeSpan delay, object parameters = null)
        {
            var task = new Task(() => job.Execute(parameters), delay.TotalMilliseconds, false);
            task.Launch();
            return task;
        }

        public Task SchedulePeriodicJob(IJob job, TimeSpan interval, object parameters = null)
        {
            var task = new Task(() => job.Execute(parameters), interval.TotalMilliseconds, true);
            task.Launch();
            return task;
        }

        public Task ScheduleAction(Action action, TimeSpan delay, bool repeat = false)
        {
            var task = new Task(action, delay.TotalMilliseconds, repeat);
            task.Launch();
            return task;
        }
    }    
}